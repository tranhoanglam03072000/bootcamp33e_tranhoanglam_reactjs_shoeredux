import logo from "./logo.svg";
import "./App.css";
import LayoutShoeShop from "./redux_shoe/LayoutShoeShop";

function App() {
  return (
    <div className="App">
      <LayoutShoeShop />
    </div>
  );
}

export default App;
