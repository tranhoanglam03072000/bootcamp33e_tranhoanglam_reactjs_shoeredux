import React, { Component } from "react";
import { connect } from "react-redux";
import CartShoe from "./CartShoe";
import ItemShoe from "./ItemShoe";

class LayoutShoeShop extends Component {
  renderListShoe = () => {
    return this.props.dataShoes.map((shoe, index) => {
      return <ItemShoe shoe={shoe} key={index} />;
    });
  };
  render() {
    return (
      <div className="container">
        <div className="d-flex justify-content-end">
          <CartShoe />
        </div>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    dataShoes: state.shoeReducer.dataShoes,
  };
};
export default connect(mapStateToProps)(LayoutShoeShop);
