import React, { Component } from "react";
import { connect } from "react-redux";
import {
  handleChangeQuantityAction,
  handleConfirmPayAction,
  handleRemoveShoeCartAction,
} from "./redux/action/shoesShopAction";
class CartShoe extends Component {
  renderTbody = () => {
    return this.props.cart.map((sanPham, index) => {
      return (
        <tr key={index}>
          <th>{sanPham.name}</th>
          <td>
            <img src={sanPham.image} width={100} alt="" />
          </td>
          <td>{sanPham.price} $</td>
          <td>
            <button
              onClick={() => this.props.handleChangeQuantity(-1, sanPham.id)}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{sanPham.soLuong}</span>
            <button
              onClick={() => this.props.handleChangeQuantity(1, sanPham.id)}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>{(sanPham.price * sanPham.soLuong).toLocaleString()} $</td>
          <td>
            <button
              onClick={() => this.props.handleRemoveShoeCart(sanPham.id)}
              className="btn btn-danger"
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <div>
          {/* Button trigger modal */}
          <button
            type="button"
            className="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            <i className="fa fa-shopping-cart" /> (
            {this.props.cart
              .reduce((tongSoLuong, shoe, index) => {
                return (tongSoLuong += shoe.soLuong);
              }, 0)
              .toLocaleString()}
            )
          </button>
          {/* Modal */}
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex={-1}
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-fullscreen">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="exampleModalLabel">
                    Shop tốn tiền
                  </h1>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  />
                </div>
                <div className="modal-body">
                  <table className="table">
                    <thead>
                      <tr>
                        <th>Tên</th>
                        <th>Hình ảnh</th>
                        <th>Giá</th>
                        <th>Số lượng</th>
                        <th>Thành tiên</th>
                        <th>Tuỳ chọn</th>
                      </tr>
                    </thead>
                    <tfoot>
                      {this.props.cart.length > 0 && (
                        <tr>
                          <th colSpan={2}>tổng tiền</th>
                          <th colSpan={4}>
                            {this.props.cart
                              .reduce((tongtien, shoe, index) => {
                                return (tongtien += shoe.soLuong * shoe.price);
                              }, 0)
                              .toLocaleString()}{" "}
                            $
                          </th>
                        </tr>
                      )}
                    </tfoot>
                    <tbody>{this.renderTbody()}</tbody>
                  </table>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <button
                    onClick={this.props.handleConfirmPay}
                    type="button"
                    className="btn btn-primary"
                  >
                    xác nhận mua
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (value, shoeId) => {
      dispatch(handleChangeQuantityAction(value, shoeId));
    },
    handleRemoveShoeCart: (shoeId) => {
      dispatch(handleRemoveShoeCartAction(shoeId));
    },
    handleConfirmPay: () => {
      dispatch(handleConfirmPayAction());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
