import React, { Component } from "react";
import { connect } from "react-redux";
import { handleAddToCartAction } from "./redux/action/shoesShopAction";

class ItemShoe extends Component {
  render() {
    let { shoe } = this.props;
    return (
      <div className=" col-4 my-5">
        <div className="card" style={{ width: "100%" }}>
          <img
            src={shoe.image}
            className="card-img-top"
            style={{ width: "300px" }}
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">{shoe.name}</h5>
            <p className="card-text"></p>
            <button
              onClick={() => this.props.handleAddToCart(shoe)}
              className="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      dispatch(handleAddToCartAction(shoe));
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
