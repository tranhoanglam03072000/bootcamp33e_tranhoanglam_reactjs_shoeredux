import { data_shoes } from "../../data/data";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  CONFIRM_PAY,
  REMOVE_SHOE,
} from "../constant/shoesShopConstant";
import Swal from "sweetalert2";

const initial = {
  dataShoes: data_shoes,
  cart: [],
};
export const shoeReducer = (state = initial, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART:
      {
        let newCart = [...state.cart];
        let index = state.cart.findIndex((shoe) => shoe.id == payload.id);
        if (index == -1) {
          newCart.push({ ...payload, soLuong: 1 });
        } else {
          newCart[index].soLuong++;
        }
        state.cart = newCart;
        return { ...state };
      }
      break;
    case CHANGE_QUANTITY:
      {
        let newCart = [...state.cart];
        let index = newCart.findIndex((shoe) => payload.shoeId == shoe.id);
        if (index != -1) {
          newCart[index].soLuong += payload.value;
          newCart[index].soLuong <= 0 && newCart.splice(index, 1);
        }
        state.cart = newCart;
        return { ...state };
      }
      break;
    case REMOVE_SHOE:
      {
        let index = state.cart.findIndex((shoe) => shoe.id == payload);
        if (index != -1) {
          state.cart.splice(index, 1);
        }
        return { ...state, cart: [...state.cart] };
      }
      break;
    case CONFIRM_PAY: {
      if (state.cart.length == 0) {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Mua Thất Bại",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Mua thành công",
          showConfirmButton: true,
          timer: 1500,
        });
        state.cart = [];
      }
      return { ...state };
    }
    default:
      return { ...state };
  }
};
