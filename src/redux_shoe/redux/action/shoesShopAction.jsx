import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  CONFIRM_PAY,
  REMOVE_SHOE,
} from "../constant/shoesShopConstant";

export const handleAddToCartAction = (shoe) => {
  return {
    type: ADD_TO_CART,
    payload: shoe,
  };
};
export const handleChangeQuantityAction = (value, shoeId) => {
  return {
    type: CHANGE_QUANTITY,
    payload: {
      value,
      shoeId,
    },
  };
};
export const handleRemoveShoeCartAction = (shoeId) => {
  return {
    type: REMOVE_SHOE,
    payload: shoeId,
  };
};

export const handleConfirmPayAction = () => {
  return {
    type: CONFIRM_PAY,
  };
};
