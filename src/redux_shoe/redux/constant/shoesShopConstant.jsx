export const ADD_TO_CART = "ADD_TO_CART";
export const CHANGE_QUANTITY = "CHANGE_QUANTITY";
export const REMOVE_SHOE = "REMOVE_SHOE";
export const CONFIRM_PAY = "CONFIRM_PAY";
